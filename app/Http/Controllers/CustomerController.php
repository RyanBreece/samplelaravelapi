<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function postCustomer(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:customers',
            'phone' => 'required',
            'street-address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'country' => 'required',
            'account-number' => 'required|unique:customers'
        ]);

        $customer = new Customer([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'street-address' => $request->input('street-address'),
            'city' => $request->input('city'),
            'zipcode' => $request->input('zipcode'),
            'country' => $request->input('country'),
            'account-number' => $request->input('account-number'),
        ]);

        $customer->save();

        return response()->json([
            'message' => 'Customer created successfully!'
        ], 201);
    }

    public function getCustomer()
    {
        $customers = Customer::paginate(10);
        return response()->json([
            'customers' => $customers
        ], 200);
    }

    public function putCustomer(Request $request, $id)
    {
        $customer = Customer::find($id);
        if (!$customer) {
            return response()->json([
                'error' => 'Customer Not Found'
            ], 404);
        }

        $customer->fill($request->all())->save();

        return response()->json([
            'message' => 'Customer update successful!'
        ], 200);
    }

    public function deleteCustomer($id)
    {
        $customer = Customer::find($id);
        if (! $customer) {
            return response()->json([
                'error' => 'Customer not found!'
            ], 404);
        }
        $customer->delete();
        return response()->json([
            'message' => 'Customer deleted!'
        ], 200);
    }
}
