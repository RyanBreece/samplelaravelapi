<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user', [
    'uses' => 'UserController@signUp'
]);

Route::post('/user/signin', [
    'uses' => 'UserController@signIn'
]);

Route::post('/customer', [
    'uses' => 'CustomerController@postCustomer',
    'middleware' => 'auth.jwt'
]);

Route::get('/customer', [
    'uses' => 'CustomerController@getCustomer',
    'middleware' => 'auth.jwt'
]);

Route::put('/customer/{id}', [
    'uses' => 'CustomerController@putCustomer',
    'middleware' => 'auth.jwt'
]);

Route::delete('/customer/{id}', [
    'uses' => 'CustomerController@deleteCustomer',
    'middleware' => 'auth.jwt'
]);